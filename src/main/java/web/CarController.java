package web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import mapper.CarMapper;
import pojo.Car;
import service.CarService;

@Controller
@RequestMapping(value="/car")
public class CarController {
	@Autowired
	private CarService carService;
	
	@Autowired
	private CarMapper carMapper;
	//,consumes = "application/json;charset=utf-8" 
	/*@RequestMapping(value="/create",method = RequestMethod.POST)
	@ResponseBody
	public boolean create(@RequestBody int id,@RequestBody String longitude,@RequestBody String latitude) {
		Car car = new Car();
		car.setId(id);
		car.setLatitude(latitude);
		car.setLongitude(longitude);
		carMapper.insert(car);
		return true;
	}
	*/
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Car test(@PathVariable int id) {
		//carMapper.insert(car);
		Car car = carMapper.selectByPrimaryKey(id);
		return car;
	}
}
